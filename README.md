# Simulation



## Simulation framework

Basilisk with Vizard visualization tool was used as a simulation framework. The main target of simulation was visualization of satellite.

## Installing Basilisk Vizard 

First step is to install Basilisk itself, as it is the base for the visualization, that can be done according to the link below:
- [Basilsk installation](https://hanspeterschaub.info/basilisk/Install.html)

Next step is downloading Vizard:
- [Vizard download](https://hanspeterschaub.info/basilisk/Vizard/VizardDownload.html)

## Using Basilsk and Vizard
At the beginning, the most helpful is 'Learing Basilsk' tab on project site:
- [Learing Basilisk](https://hanspeterschaub.info/basilisk/Learn.html)

And the 'User Guide' of Vizard:
- [Vizard user guide](https://hanspeterschaub.info/basilisk/Vizard/VizardGUI.html) with special attention to 'Import a Custom Shape Model' section.

## Satellite 3D model in simulation
![Cubesat ](Cubesat_simulation.png)
